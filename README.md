Тестовое задание:

### Запуск контейнеров:
```bash
./vendor/laravel/sail/bin/sail up -d
./vendor/laravel/sail/bin/sail artisan migrate
```
### Завести новый токен:
```bash
php artisan app:new-token
```
### Запрос из первого задания:
```bash
php artisan app:first
```
