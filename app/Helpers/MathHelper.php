<?php

namespace App\Helpers;

class MathHelper
{

    public static function calculateCommission(float $value, bool $mode = true): float
    {
        return $mode ? ($value + ((2/100) * $value)) : ($value - ((2/100) * $value));
    }
}
