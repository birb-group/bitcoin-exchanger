<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use RyanChandler\Bearer\Models\Token;

class NewToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:new-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $str = Str::random(32);
        Token::create([
            'token' => $str,
        ]);
        $this->info('you token ' . $str);
        //
    }
}
