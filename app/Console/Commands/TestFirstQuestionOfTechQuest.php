<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TestFirstQuestionOfTechQuest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:first';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Первое задание, поместил сюда что бы не постить лишних ссылок с решением в интернет';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->info('result', DB::select('select u.first_name as first_name, u.last_name as last_name, bk1.author as author, bk1.name as book1, bk2.name as book2 from books bk1
        left join books bk2 ON bk2.author = bk1.author
        inner join `user_books` ub on bk1.`id` = ub.`book_id`
        inner join `users` u on bk1.`id` = ub.`user_id`
where
        FLOOR(DATEDIFF(NOW(), u.birthday) / 365.25) >= 7 and FLOOR(DATEDIFF(NOW(), u.birthday) / 365.25) <= 17 and
        ub.return_date >= ub.get_date - interval 2 WEEK
GROUP BY bk1.author HAVING COUNT(bk1.author) = 2;'));
        //
    }
}
