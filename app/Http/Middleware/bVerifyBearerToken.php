<?php

namespace App\Http\Middleware;

use App\DTO\FailDTO;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use RyanChandler\Bearer\Bearer;

class bVerifyBearerToken
{
    protected Bearer $bearer;

    public function __construct(Bearer $bearer)
    {
        $this->bearer = $bearer;
    }

    public function handle(Request $request, Closure $next)
    {
        $token = $this->findTokenStringFromRequest($request);

        if (! is_string($token)) {
            return $token;
        }

        $token = $this->bearer->find($token);

        if (! $token) {
            return $this->invalidToken('Invalid token');
        }

        if ($token->expired) {
            return $this->invalidToken('This token has expired');
        }

        if (! config('bearer.verify_domains') || ! $token->domains || $token->domains->collect()->isEmpty()) {
            return $next($request);
        }

        if (! in_array($request->getSchemeAndHttpHost(), $token->domains->toArray())) {
            return $this->invalidToken('This token cannot be used with your domain.');
        }

        return $next($request);
    }

    protected function findTokenStringFromRequest(Request $request)
    {
        if ($request->has('token')) {
            return $request->input('token');
        }

        $token = $request->header('Authorization');

        if (! $token) {
            return $this->invalidToken('Invalid token');
        }

        $token = Str::after($token, 'Bearer ');

        if (! $token) {
            return $this->invalidToken('Invalid token');
        }

        return $token;
    }

    private function invalidToken(string $string): JsonResponse
    {
        return response()->json((FailDTO::from([
            'error' => 403,
            'message' => $string
        ]))->toArray(), 403);
    }
}
