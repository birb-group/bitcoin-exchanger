<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use RyanChandler\Bearer\Bearer;
use RyanChandler\Bearer\Models\Token;
use Symfony\Component\HttpFoundation\Response;

class TokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if($request->hasHeader('token')) {
            $token = Bearer::find($request->get('token'));
            $token->addDays(1)->save();
        }
        return $next($request);
    }
}
