<?php

namespace App\Http\Controllers;

use App\DTO\ConvertSuccessDTO;
use App\DTO\FailDTO;
use App\DTO\RateWithCommissions;
use App\DTO\SuccessDTO;
use App\Helpers\MathHelper;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ExchangeController extends Controller
{
    public function v1(Request $request): array {
        try {
            $request->validate(['method' => 'required']);
            return match ($request->get('method')) {
                'rates' => $this->rates($request),
                'convert' => $this->convert($request),
                default => (FailDTO::from([
                    'code' => '404',
                    'message' =>'not found method'
                ]))->toArray(),
            };
        } catch (GuzzleException $e) {
            return (new FailDTO('Request to blockchain.info failed. Guzzle error, '. $e->getMessage(), $e->getTrace()))->toArray();
        } catch (Exception $e) {
            return (new FailDTO('Other error, '. $e->getMessage(), $e->getTrace()))->toArray();
        }
    }

    /**
     * @param Request $request
     * @return array
     * @throws GuzzleException
     */
    private function rates(Request $request): array {
        $rates = $this->getRates();
        if($request->has('currency') && ($rate = $request->get('currency'))) {
            $rates = collect($rates[$rate]);
        }
        return (new SuccessDTO(
            $rates->sortBy('sell')->toArray()
        ))->toArray();
    }

    /**
     * @param Request $request
     * @return array
     * @throws GuzzleException
     */
    private function convert(Request $request): array {
        $request->validate([
            'currency_from' => 'required',
            'currency_to' => 'required',
        ]);
        $from = $request->get('currency_from');
        $value = $request->get('value');
        $isToBTC = $from !== 'BTC';
        if($isToBTC && $value < 0.01) {
            return FailDTO::from(['message' => 'min to convert 0.01 ' . $from, 'code' => 400])->toArray();
        }
        $to = $request->get('currency_to');
        if($from === $to) {
            return FailDTO::from(['message' => 'convert currency to himself - unsupported. Invalid request', 'code' => 404])->toArray();
        }
        $rates = $this->getRates();
        $client = new Client();
        if($isToBTC) {
            $rate = $rates[$from];
            $result = $client->request('GET', "https://blockchain.info/tobtc?currency={$from}&value={$value}")->getBody()->getContents();
        } else {
            $rate = $rates[$to];
            $result = ($rate->sell * $value);
        }
        return (new SuccessDTO(
            (new ConvertSuccessDTO(
                floatval($result),
                MathHelper::calculateCommission(floatval($result), false),
                $rate->sell,
                $from,
                $to
            ))->toArray()
        ))->toArray();
    }

    /**
     * @return Collection<RateWithCommissions>
     * @throws GuzzleException
     */
    private function getRates(): Collection
    {
        $client = new Client();
        $response = json_decode($client->request('GET', 'https://blockchain.info/ticker')->getBody(), true);
        return collect($response)->map(function (array $rates) {
            return RateWithCommissions::from($rates);
        });
    }
}
