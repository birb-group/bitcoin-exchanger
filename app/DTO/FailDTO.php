<?php

namespace App\DTO;

use Illuminate\Support\Facades\Log;
use Spatie\LaravelData\Data;

class FailDTO extends Data
{

    public int $code = 500;
    public string $status;
    public function __construct(
        public string $message,
        private readonly ?array $trace = []
    )
    {
        Log::error($this->message, $this->trace);
        if($this->code !== 200) {
            $this->status = 'error';
        }
    }
}
