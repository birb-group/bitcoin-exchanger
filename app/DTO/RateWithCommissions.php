<?php

namespace App\DTO;

use App\DTO\Transformers\AddCommissionTransformer;
use Spatie\LaravelData\Attributes\MapInputName;
use Spatie\LaravelData\Attributes\MapOutputName;
use Spatie\LaravelData\Attributes\WithTransformer;
use Spatie\LaravelData\Data;

class RateWithCommissions extends Data
{
    public function __construct(
        #[MapOutputName('15m')]
        #[MapInputName('15m')]
        #[WithTransformer(AddCommissionTransformer::class)]
        public float $ism,
        #[WithTransformer(AddCommissionTransformer::class)]
        public float $last,
        #[WithTransformer(AddCommissionTransformer::class)]
        public float $buy,
        #[WithTransformer(AddCommissionTransformer::class)]
        public float $sell,
        public string $symbol,
    )
    {
    }
}
