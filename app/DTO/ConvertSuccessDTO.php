<?php

namespace App\DTO;


class ConvertSuccessDTO extends SuccessDataDTO
{

    public function __construct(
        public float $value,
        public float $converted_value,
        public float $rate,
        public string $currency_from,
        public string $currency_to
    )
    {
    }

}
