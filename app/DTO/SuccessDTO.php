<?php

namespace App\DTO;

use Spatie\LaravelData\Data;

class SuccessDTO extends Data
{

    public string $status = 'success';
    public int $code = 200;

    public function __construct(
        public array $data
    )
    {
    }
}
