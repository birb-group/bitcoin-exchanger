<?php

namespace App\DTO\Transformers;

use App\Helpers\MathHelper;
use Spatie\LaravelData\Support\DataProperty;
use Spatie\LaravelData\Transformers\Transformer;

class AddCommissionTransformer implements Transformer
{
    public function transform(DataProperty $property, mixed $value): mixed
    {
        return is_float($value) ? round(MathHelper::calculateCommission($value), 10) : $value;
    }
}


