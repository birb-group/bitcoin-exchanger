<?php

namespace App\Models;

use App\Models\Scopes\WithTwoSameAuthors;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBook extends Model
{
    use HasFactory;
    public $timestamps;
    protected $with = [''];

    protected static function boot()
    {
        static::addGlobalScope(new WithTwoSameAuthors);
        parent::boot();
    }

}
