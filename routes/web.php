<?php

use App\Http\Controllers\ExchangeController;
use App\Http\Middleware\bVerifyBearerToken;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/api'], function () {
    Route::match(['GET', 'POST'], '/v1', [ExchangeController::class, 'v1'])
        ->middleware(bVerifyBearerToken::class);
});
